import React, { Component } from "react";
import "./App.css";

import Step from "./Components/Step/Step";
import Total from "./Components/Total/Total";
import Message from "./Components/Message/Message";

class App extends Component {
  constructor() {
    super();

    this.postData = this.postData.bind(this);
    this.setupGlobalStateChangers = this.setupGlobalStateChangers.bind(this);
    this.setupStepStateChangers = this.setupStepStateChangers.bind(this);
    this.setupStepGlobalPassing = this.setupStepGlobalPassing.bind(this);
    this.updateTotal = this.updateTotal.bind(this);
    this.goToStep = this.goToStep.bind(this);
    this.incrementStep = this.incrementStep.bind(this);
    this.decrementStep = this.decrementStep.bind(this);
    this.isStepChangeValid = this.isStepChangeValid.bind(this);
    this.addToSelectedProducts = this.addToSelectedProducts.bind(this);
    this.removeFromSelectedProducts = this.removeFromSelectedProducts.bind(
      this
    );
    this.renderStepButtons = this.renderStepButtons.bind(this);
    this.renderStepButton = this.renderStepButton.bind(this);
    this.renderSteps = this.renderSteps.bind(this);
    this.renderStep = this.renderStep.bind(this);
  }
  componentDidMount() {
    fetch(window.location.href + "wizard-config")
      .then(response => {
        return response.json();
      })
      .then(response => {
        this.steps = response.Steps;
        this.setState(this.setupGlobalState(response), () => {
          this.setupStepGlobalPassing();
          this.setupGlobalStateChangers(response.Globals);
          this.setupStepStateChangers();
        });
      });
  }
  componentDidUpdate() {
    this.setupStepGlobalPassing();
  }

  postData() {
    //TODO: build this out when given a schema
    const { data, url } = this.state.wizardData.Post;
    let postData = {};

    for (let i = 0; i < data.length; i++) {
      postData[data[i].label] = this.state[data[i].fieldName];
    }

    postData["Total Price"] = this.state.totalPrice;
    postData["Selected Products"] = this.state.selectedProducts;

    fetch(url, {
      method: "POST", // or 'PUT'
      body: JSON.stringify(postData),
      headers: new Headers({
        "Content-Type": "application/json"
      })
    })
      .then(res => res.json())
      .catch(error => console.error("Error:", error))
      .then(response => {
        this.setState({
          posted: true
        });
      });
  }

  setupGlobalState(Wizard) {
    let state = {
      selectedProducts: [],
      wizardData: Wizard,
      currentStep: 0,
      totalPrice: 0,
      posted: false
    };

    for (let i = 0; i < Wizard.Globals.length; i++) {
      if (Wizard.Globals[i].type === "number") {
        state[Wizard.Globals[i].name] = Wizard.Globals[i].min
          ? Wizard.Globals[i].min
          : 0;
      } else if (Wizard.Globals[i].type === "bool") {
        state[Wizard.Globals[i].name] = false;
      } else {
        state[Wizard.Globals[i].name] = Wizard.Globals[i].default;
      }
    }

    return state;
  }

  setupGlobalStateChangers(globals) {
    for (let i = 0; i < globals.length; i++) {
      this[globals[i].name] = function(value) {
        let state = {};
        let previousValue = this.state[globals[i].name];
        let priceValue = 0; //used if we modify overall total

        //need to remove old value from price then add new value
        if (globals[i].modifiesPrice) {
          //find difference in price
          priceValue = value - previousValue;
          state.totalPrice = this.state.totalPrice + priceValue;
        }

        state[globals[i].name] = value;
        this.setState(state);
      };
      this[globals[i].name] = this[globals[i].name].bind(this);
    }
  }

  setupStepStateChangers() {
    for (let i = 0; i < this.steps.length; i++) {
      let step = this.steps[i];
      for (let j = 0; j < step.components.length; j++) {
        let componentData = step.components[j];
        if (componentData.onChange) {
          for (let i = 0; i < componentData.onChange.length; i++) {
            componentData.onChange[i].setState = this[
              componentData.onChange[i].setState
            ];
          }
        }

        if (componentData.valid && componentData.valid.onChange) {
          for (let i = 0; i < componentData.valid.onChange.length; i++) {
            componentData.valid.onChange[i].setState = this[
              componentData.valid.onChange[i].setState
            ];
          }
        }
      }
    }
  }

  setupStepGlobalPassing() {
    for (let i = 0; i < this.steps.length; i++) {
      let step = this.steps[i];
      for (let j = 0; j < step.components.length; j++) {
        let componentData = step.components[j];
        if (componentData.type === "product") {
          componentData.globalReference = this.state[componentData.globalValue];
        }
      }
    }
  }

  updateTotal(amount) {
    let totalprice = this.state.totalPrice;
    totalprice += amount;
    this.setState({ totalPrice: totalprice });
  }

  goToStep(step) {
    if (this.isStepChangeValid(step)) {
      this.setState({ currentStep: step });
    }
  }

  incrementStep() {
    if (this.state.currentStep < this.steps.length) {
      let newStep = this.state.currentStep;
      newStep++;
      this.setState({ currentStep: newStep });
    }
  }

  decrementStep() {
    if (this.state.currentStep > 0) {
      let newStep = this.state.currentStep;
      newStep--;
      this.setState({ currentStep: newStep });
    }
  }

  isStepChangeValid(step) {
    if (this[`step-${step}`]) {
      if (step === 0) {
        return true;
      }
      var changeValid = true;
      for (var i = 0; i < step; i++) {
        if (!this[`step-${i}`].state.valid) {
          changeValid = false;
        }
      }
      return changeValid;
    } else {
      return true;
    }
  }

  addToSelectedProducts(product) {
    let selectedProducts = this.state.selectedProducts;
    selectedProducts.push(product);
    return this.setState({
      selectedProducts: selectedProducts
    });
  }

  removeFromSelectedProducts(product) {
    let indexToDelete = -1;
    let selectedProducts = this.state.selectedProducts;
    for (let i = 0; i < selectedProducts.length; i++) {
      if (selectedProducts[i].title === product.title) {
        indexToDelete = i;
      }
    }
    selectedProducts.splice(indexToDelete, 1);
    return this.setState({
      selectedProducts: selectedProducts
    });
  }

  renderStepButtons() {
    let steps = [];
    for (let i = 0; i < this.steps.length; i++) {
      steps.push(this.renderStepButton(i));
    }
    return steps;
  }

  renderStepButton(key) {
    let className = "step-button";
    if (key === this.state.currentStep) {
      className += " active";
    } else if (key <= this.state.currentStep) {
      className += " complete";
    }

    return (
      <button
        tabIndex="-1"
        className={className}
        type="button"
        key={key}
        onClick={e => {
          this.goToStep(key);
        }}
      >
        Step {key + 1}
      </button>
    );
  }

  renderSteps() {
    if (this.steps) {
      let steps = [];
      for (let i = 0; i < this.steps.length; i++) {
        steps.push(this.renderStep(this.steps[i], i));
      }
      return steps;
    }
  }

  renderStep(step, key) {
    const stepClass = {
      display: key === this.state.currentStep ? "inline" : "none"
    };
    return (
      <Step
        style={stepClass}
        key={key}
        config={step}
        stepNumber={this.state.currentStep}
        numOfSteps={this.steps.length}
        incrementStep={this.incrementStep}
        decrementStep={this.decrementStep}
        updateTotal={this.updateTotal}
        post={this.postData}
        addToSelectedProducts={this.addToSelectedProducts}
        removeFromSelectedProducts={this.removeFromSelectedProducts}
        ref={step => {
          this[`step-${key}`] = step;
        }}
      />
    );
  }

  render() {
    return (
      <div id="wizard">
        {this.state &&
          !this.state.posted && (
            <div>
              {this.steps && (
                <div className="step-buttons">{this.renderStepButtons()}</div>
              )}
              {this.renderSteps()}
              {this.state &&
                this.state.totalPrice && (
                  <Total total={this.state.totalPrice} />
                )}
            </div>
          )}
        {this.state &&
          this.state.posted && (
            <div className="success-message">
              <Message text={this.state.wizardData.Post.successMessage} />
            </div>
          )}
      </div>
    );
  }
}

export default App;
