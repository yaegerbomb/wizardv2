import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Link extends Component {
    onClick = () =>{
        if(this.props.onChange){
            for(let i = 0; i < this.props.onChange.length; i++){
                if(this.props.onChange[i].value === "value"){
                    this.props.onChange[i].setState(true)
                }else{
                    this.props.onChange[i].setState(this.state[this.props.onChange[i].value]);
                }
            }
        }
    }
    render(){
        return(
            <div>
                <a href={this.props.url} target={this.props.opens} onClick={(e) => this.onClick()}>
                    {this.props.text ? this.props.text : this.props.url}
                </a>
            </div>
        )
    }
}

Link.propTypes = {
    opens: PropTypes.string,
    text: PropTypes.string,
    url: PropTypes.string.isRequired,
    onChange: PropTypes.array.isRequired
}

export default Link;