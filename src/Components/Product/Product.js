import React, { Component } from "react";
import PropTypes from "prop-types";

import DOMPurify from "dompurify";

import Number from "../Number/Number";

class Product extends Component {
  constructor() {
    super();

    this.checkIfValid = this.checkIfValid.bind(this);
    this.modifyOtherQuantity = this.modifyOtherQuantity.bind(this);
    this.calculatePrice = this.calculatePrice.bind(this);
    this.selectProduct = this.selectProduct.bind(this);
    this.deSelectProduct = this.deSelectProduct.bind(this);

    this.state = {
      totalPrice: 0,
      otherQuantity: 0,
      oldQuantity: 0,
      isValid: false
    };
  }
  componentDidMount() {
    if (!this.props.byGlobal && this.props.otherQuantity) {
      this.setState({ otherQuantity: this.props.otherQuantity });
    } else {
      this.setState({ isValid: true });
    }
  }
  componentDidUpdate() {
    var valid = this.checkIfValid();
    if (this.state.isValid !== valid) {
      this.setState({ isValid: valid });
    }
  }
  componentWillReceiveProps() {
    this.calculatePrice(this.props.selected);
  }
  checkIfValid() {
    let valid = true;

    if (this.props.selected) {
      if (this.otherQuantityNum && !this.otherQuantityNum.state.isValid) {
        valid = false;
      }
    }
    return valid;
  }
  modifyOtherQuantity(value) {
    if (value) {
      let oldQuantity = this.props.selected ? this.state.otherQuantity : 0;
      if (oldQuantity < 0) {
        oldQuantity = 0;
      }
      this.setState(
        {
          otherQuantity: value,
          oldQuantity: oldQuantity
        },
        () => {
          this.calculatePrice(this.props.selected);
        }
      );
    }
  }
  calculatePrice(selected) {
    let total = 0;
    if (selected) {
      let quantityTotal = false;
      if (this.props.byGlobal) {
        total = this.props.globalReference * this.props.price;
      } else {
        total = this.state.otherQuantity * this.props.price;
        if (total < 0) {
          total = 0;
        }
        quantityTotal = true;
      }

      if (this.props.minCharge && total < this.props.minCharge) {
        total = this.props.minCharge;
      }

      if (this.props.maxCharge && total > this.props.maxCharge) {
        total = this.props.maxCharge;
      }
      if (total !== this.state.totalPrice) {
        this.setState({ totalPrice: total }, () => {
          if (quantityTotal) {
            total -= this.state.oldQuantity * this.props.price;
            const quantity =
              this.state.otherQuantity >= 0 ? this.state.otherQuantity : 0;
            this.setState({ oldQuantity: quantity });
          }
          this.props.updateTotal(total);
        });
      }
    } else if (this.state.totalPrice !== 0) {
      let oldPrice = this.state.totalPrice;
      this.setState({ totalPrice: 0 }, () => {
        this.setState({ oldQuantity: 0 });
        this.props.updateTotal(-oldPrice);
      });
    }
  }
  selectProduct(key) {
    this.props.selectProduct(key);
    this.calculatePrice(true);
  }
  deSelectProduct(key) {
    this.props.deSelectProduct(key);
    this.calculatePrice(false);
  }
  render() {
    let productClass = "c-product";
    if (this.props.selected) {
      productClass += " selected";
    }
    return (
      <div className={productClass}>
        <div className="c-product-title">{this.props.title}</div>
        <div
          className="c-product-description"
          dangerouslySetInnerHTML={{
            __html: DOMPurify.sanitize(this.props.description)
          }}
        />
        {!this.props.byGlobal &&
          this.props.showOtherQuantity && (
            <Number
              min={this.props.otherQuantityMin}
              max={this.props.otherQuantityMax}
              default={this.props.otherQuantity}
              required={this.props.selected}
              label={this.props.otherValueLabel}
              onChange={[
                {
                  setState: this.modifyOtherQuantity,
                  value: "value"
                }
              ]}
              ref={num => {
                this.otherQuantityNum = num;
              }}
            />
          )}
        {this.props.priceVisible && (
          <div className="c-product-price">
            <div className="c-product-price-label">Price: </div>
            ${this.props.price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,")}
          </div>
        )}
        {!this.props.selected && (
          <button
            className="button button-add"
            type="button"
            onClick={() => this.selectProduct(this.props.selectId)}
          >
            Add
          </button>
        )}
        {this.props.selected && (
          <button
            className="button button-remove"
            type="button"
            onClick={() => this.deSelectProduct(this.props.selectId)}
          >
            Remove
          </button>
        )}
      </div>
    );
  }
}

Product.propTypes = {
  selectId: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.string,
  price: PropTypes.number,
  priceVisible: PropTypes.bool,
  minCharge: PropTypes.number,
  maxCharge: PropTypes.number,
  byGlobal: PropTypes.bool,
  globalValue: PropTypes.any,
  globalReference: PropTypes.any,
  otherValueLabel: PropTypes.string,
  otherQuantity: PropTypes.number,
  otherQuantityMin: PropTypes.number,
  otherQuantityMax: PropTypes.number,
  showOtherQuantity: PropTypes.bool,
  updateTotal: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
  selectProduct: PropTypes.func.isRequired,
  deSelectProduct: PropTypes.func.isRequired
};

export default Product;
