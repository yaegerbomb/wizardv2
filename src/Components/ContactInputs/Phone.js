import React, { Component } from "react";
import PropTypes from "prop-types";
import InputMask from "react-input-mask";

import UniqueID from "../../Utils/UniqueId";

class Phone extends Component {
  constructor() {
    super();

    this.state = {
      value: "",
      mask: "(999) 999-9999",
      isValid: false
    };
  }
  componentWillMount() {
    this.id = UniqueID();
  }
  isValidValue = newValue => {
    let isValid = true;
    if (!newValue || newValue.indexOf("_") !== -1) {
      isValid = false;
    }

    this.setState({ isValid: isValid });

    return isValid;
  };

  changeValue = e => {
    this.isValidValue(e.target.value);
    for (let i = 0; i < this.props.onChange.length; i++) {
      if (this.props.onChange[i].value === "value") {
        this.props.onChange[i].setState(e.target.value);
      } else {
        this.props.onChange[i].setState(
          this.state[this.props.onChange[i].value]
        );
      }
    }
    this.setState({ value: e.target.value });
  };
  render() {
    return (
      <div className="c-form-group contact-input">
        <label className="c-form-label" htmlFor={this.id}>
          Phone
        </label>
        <InputMask
          className="c-form-input"
          {...this.props}
          mask="(999) 999-9999"
          onChange={this.changeValue}
          required
          placeholder="Phone"
        />
      </div>
    );
  }
}

Phone.propTypes = {
  onChange: PropTypes.array.isRequired
};

export default Phone;
