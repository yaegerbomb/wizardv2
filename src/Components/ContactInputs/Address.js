import React, { Component } from "react";
import PropTypes from "prop-types";

import UniqueID from "../../Utils/UniqueId";
import Message from "../Message/Message";

class Address extends Component {
  constructor() {
    super();

    this.isValidValue = this.isValidValue.bind(this);
    this.changeValue = this.changeValue.bind(this);
    this.getProperties = this.getProperties.bind(this);
    this.checkCustomValueIsValid = this.checkCustomValueIsValid.bind(this);
    this.checkOneOfValuesExists = this.checkOneOfValuesExists.bind(this);

    this.state = {
      inputValue: "",
      isValid: false,
      showInvalidMessage: false
    };
  }
  componentWillMount() {
    this.id = UniqueID();
  }
  componentDidMount() {
    if (this.props.default) {
      this.setState({
        inputValue: this.props.default,
        isValid: this.isValidValue(this.props.default)
      });
    }
  }
  isValidValue(newValue) {
    let isValid = true;
    if (true) {
      if (!newValue) {
        isValid = false;
      }
    }

    const customValidation = this.checkCustomValueIsValid(newValue);
    if (!customValidation) {
      isValid = false;
    }

    this.setState({ isValid: isValid });

    //check if we should show invalid message
    if (this.props.valid && this.props.valid.minChars) {
      const minChars = this.props.valid.minChars;
      this.setState({
        showInvalidMessage:
          newValue.length >= minChars && !isValid ? true : false
      });
    }

    return isValid;
  }
  checkCustomValueIsValid(newValue) {
    if (this.props.valid) {
      //each of these should check if our custom valid rule needs to set some other state
      switch (this.props.valid.type) {
        case "oneOf":
          return this.checkOneOfValuesExists(newValue);
        default:
          return true;
      }
    } else {
      return true;
    }
  }
  checkOneOfValuesExists(newValue) {
    const valid = this.props.valid.values[newValue];
    if (valid && this.props.valid.onChange) {
      for (let i = 0; i < this.props.valid.onChange.length; i++) {
        this.props.valid.onChange[i].setState(
          this.props.valid.values[newValue][this.props.valid.onChange[i].value]
        );
      }
    }
    return valid;
  }
  changeValue(e) {
    this.isValidValue(e.target.value);
    for (let i = 0; i < this.props.onChange.length; i++) {
      if (this.props.onChange[i].value === "value") {
        this.props.onChange[i].setState(e.target.value);
      } else {
        this.props.onChange[i].setState(
          this.state[this.props.onChange[i].value]
        );
      }
    }
    this.setState({ inputValue: e.target.value });
  }
  getProperties() {
    let props = {
      id: this.id,
      type: "text",
      placeholder: "Address",
      value: this.state.inputValue,
      required: true
    };

    if (this.props.onChange) {
      props.onChange = this.changeValue;
    }

    return props;
  }
  render() {
    return (
      <div className="c-form-group contact-input">
        <label className="c-form-label" htmlFor={this.id}>
          Address
        </label>
        <input className="c-form-input" {...this.getProperties()} />
        {this.state.showInvalidMessage && (
          <Message
            style={{ color: "red" }}
            text={this.props.valid.invalidMessage}
          />
        )}
      </div>
    );
  }
}

Address.propTypes = {
  default: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  valid: PropTypes.object,
  onChange: PropTypes.array.isRequired
};

export default Address;
