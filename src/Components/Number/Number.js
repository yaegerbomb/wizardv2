import React, { Component } from "react";
import PropTypes from "prop-types";

import UniqueID from "../../Utils/UniqueId";

class Number extends Component {
  constructor() {
    super();

    this.isValidValue = this.isValidValue.bind(this);
    this.changeValue = this.changeValue.bind(this);
    this.getProperties = this.getProperties.bind(this);

    this.state = {
      inputValue: "",
      isValid: false
    };
  }
  componentWillMount() {
    this.id = UniqueID();
  }
  componentDidMount() {
    if (this.props.default) {
      this.setState({
        inputValue: this.props.default,
        isValid: this.isValidValue(this.props.default)
      });
    }
  }
  isValidValue(newValue) {
    let isValid = true;
    if (this.props.min) {
      if (newValue < this.props.min) {
        isValid = false;
      }
    }

    if (this.props.max) {
      if (newValue > this.props.max) {
        isValid = false;
      }
    }

    this.setState({ isValid: isValid });
    return isValid;
  }
  changeValue(e) {
    this.isValidValue(e.target.value);
    this.setState({ inputValue: e.target.value });
    for (let i = 0; i < this.props.onChange.length; i++) {
      if (this.props.onChange[i].value === "value") {
        this.props.onChange[i].setState(e.target.value);
      } else {
        this.props.onChange[i].setState(
          this.state[this.props.onChange[i].value]
        );
      }
    }
  }
  getProperties() {
    let props = {
      id: this.id,
      type: "number",
      placeholder: this.props.placeholder ? this.props.placeholder : "",
      value: this.state.inputValue,
      required: this.props.required ? this.props.required : false
    };

    if (this.props.onChange) {
      props.onChange = this.changeValue;
    }

    return props;
  }
  render() {
    return (
      <div className="c-form-group">
        {this.props.label && (
          <label className="c-form-label" htmlFor={this.id}>
            {this.props.label}
          </label>
        )}
        <input className="c-form-input" {...this.getProperties()} />
      </div>
    );
  }
}

Number.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  default: PropTypes.number,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  label: PropTypes.string,
  onChange: PropTypes.array.isRequired
};

export default Number;
