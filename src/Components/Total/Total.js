import React, { Component } from "react";
import PropTypes from "prop-types";

class Total extends Component {
  render() {
    return (
      <div className="c-total">
        <div className="c-total-label">Total Cost: </div>
        ${this.props.total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,")}
      </div>
    );
  }
}

Total.propTypes = {
  total: PropTypes.number.isRequired
};

export default Total;
