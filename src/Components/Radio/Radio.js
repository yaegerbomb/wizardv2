import React, { Component } from 'react';
import PropTypes from 'prop-types';

import UniqueID from '../../Utils/UniqueId'

class Radio extends Component {
    constructor(){
        super();

        this.changeValue = this.changeValue.bind(this);
        this.renderRadios = this.renderRadios.bind(this);
        this.renderRadio = this.renderRadio.bind(this);

        this.state = {
            radioValue: ""
        }
    }
    componentWillMount(){
        this.id = UniqueID();

    }
    componentDidMount(){
        if(this.props.default){
            this.setRadioState(this.props.default);
        }
    }
    setRadioState(val){
        let state = {};
            
        //find val choice for radio
        let firstValue = null;
        for(let i = 0; i < this.props.values.length; i++){
            if(val === this.props.values[i].value){
                firstValue = this.props.values[i];
            }
        }

        //set our state to val values
        for(let property in firstValue){
            if (firstValue.hasOwnProperty(property)) {
                if(property !== "value"){
                    state[property] = firstValue[property];
                }else{
                    state["radioValue"] = firstValue[property]
                }
            }
        }

        this.setState(state, this.callPropChange);
    }
    changeValue(e){
        this.setRadioState(e.target.value);        
    }
    callPropChange = () =>{
        if(this.props.onChange){
            for(let i = 0; i < this.props.onChange.length; i++){
                if(this.props.onChange[i].value === "value"){
                    this.props.onChange[i].setState(this.state.radioValue)
                }else{
                    this.props.onChange[i].setState(this.state[this.props.onChange[i].value]);
                }
            }
        }
    }
    renderRadios(){
        let radios = [];
        for(let i = 0; i < this.props.values.length; i++){
            radios.push(this.renderRadio(this.props.values[i], i));
        }
        return radios;
    }
    renderRadio(radioValue, key){
        return (
            <div key={key}>
                <input 
                    type="radio" 
                    name={this.id}
                    id={radioValue.value + '-' + this.id} 
                    value={radioValue.value} 
                    checked={this.state.radioValue === radioValue.value} 
                    onChange={this.changeValue} />
                <label
                    htmlFor={radioValue.value + '-' + this.id}>
                    {radioValue.label}
                </label>
            </div>
        )
    }
    render(){
        return(
            <div>
                {this.renderRadios()}
            </div>
        )
    }
}

Radio.propTypes = {
    default: PropTypes.any,
    values: PropTypes.any,
    required: PropTypes.bool,
    label: PropTypes.string,
    onChange: PropTypes.array.isRequired
}

export default Radio;