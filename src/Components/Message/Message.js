import React, { Component } from "react";
import PropTypes from "prop-types";

class Message extends Component {
  render() {
    return (
      <div className="c-message" style={this.props.style}>
        {this.props.text}
      </div>
    );
  }
}

Message.propTypes = {
  text: PropTypes.string.isRequired,
  style: PropTypes.object
};

export default Message;
