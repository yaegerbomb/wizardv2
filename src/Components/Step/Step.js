import React, { Component } from "react";
import PropTypes from "prop-types";

import Message from "../Message/Message";
import Text from "../Text/Text";
import Number from "../Number/Number";
import Radio from "../Radio/Radio";
import Link from "../Link/Link";
import Product from "../Product/Product";
import FirstName from "../ContactInputs/FirstName";
import LastName from "../ContactInputs/LastName";
import Phone from "../ContactInputs/Phone";
import Address from "../ContactInputs/Address";
import City from "../ContactInputs/City";
import Email from "../ContactInputs/Email";

class Step extends Component {
  constructor() {
    super();

    this.selectProduct = this.selectProduct.bind(this);
    this.deSelectProduct = this.deSelectProduct.bind(this);
    this.checkStepValid = this.checkStepValid.bind(this);
    this.checkProductValidation = this.checkProductValidation.bind(this);
    this.oneOfProductValidationCheck = this.oneOfProductValidationCheck.bind(
      this
    );
    this.renderComponents = this.renderComponents.bind(this);
    this.renderProduct = this.renderProduct.bind(this);
    this.renderMessage = this.renderMessage.bind(this);
    this.renderText = this.renderText.bind(this);
    this.renderNumber = this.renderNumber.bind(this);
    this.renderRadio = this.renderRadio.bind(this);
    this.renderLink = this.renderLink.bind(this);
    this.renderFirstName = this.renderFirstName.bind(this);
    this.renderLastName = this.renderLastName.bind(this);
    this.renderPhoneNumber = this.renderPhoneNumber.bind(this);
    this.renderAddress = this.renderAddress.bind(this);
    this.renderCity = this.renderCity.bind(this);
    this.renderEmail = this.renderEmail.bind(this);

    this.state = {
      valid: false,
      selectedProducts: []
    };
  }
  componentWillMount() {
    //get number of products to keep track of selection
    let selectedProducts = [];
    for (let i = 0; i < this.props.config.components.length; i++) {
      if (this.props.config.components[i].type === "product") {
        selectedProducts.push({
          key: i,
          selected: false,
          product: this.props.config.components[i]
        });
      }
    }

    this.setState({ selectedProducts: selectedProducts });
  }
  componentDidMount() {
    var valid = this.checkStepValid();
    if (this.state.valid !== valid) {
      this.setState({ valid: valid });
    }
  }
  componentDidUpdate() {
    var valid = this.checkStepValid();
    if (this.state.valid !== valid) {
      this.setState({ valid: valid });
    }
  }
  selectProduct(key) {
    let selectedProductDataToAdd = null;
    let selectedProductDataToRemove = null;
    let selectedProducts = this.state.selectedProducts;
    for (var i = 0; i < selectedProducts.length; i++) {
      if (selectedProducts[i].key === key) {
        selectedProducts[i].selected = true;
        selectedProductDataToAdd = selectedProducts[i].product;
      } else {
        if (this.props.config.validationType === "oneOfProduct") {
          selectedProducts[i].selected = false;
        }
      }
    }
    if (selectedProductDataToAdd) {
      this.props.addToSelectedProducts(selectedProductDataToAdd);
    }

    if (selectedProductDataToRemove) {
      this.props.removeFromSelectedProducts(selectedProductDataToRemove);
    }
    this.setState({ selectedProducts: selectedProducts });
  }
  deSelectProduct(key) {
    let selectedProductDataToRemove = null;
    let selectedProducts = this.state.selectedProducts;
    for (var i = 0; i < selectedProducts.length; i++) {
      if (selectedProducts[i].key === key) {
        selectedProducts[i].selected = false;
        selectedProductDataToRemove = selectedProducts[i].product;
      }
    }
    this.props.removeFromSelectedProducts(selectedProductDataToRemove);
    this.setState({ selectedProducts: selectedProducts });
  }
  checkStepValid() {
    var valid = true;
    for (let i = 0; i < this.props.config.components.length; i++) {
      const ref = this["refs-" + i];
      if (ref) {
        if (!ref.state.isValid) {
          valid = false;
        }
      }
    }

    if (this.props.config.validationType) {
      valid = this.checkProductValidation();
    }
    return valid;
  }

  checkProductValidation() {
    switch (this.props.config.validationType) {
      case "oneOfProduct":
        return this.oneOfProductValidationCheck();
      default:
        return true;
    }
  }

  oneOfProductValidationCheck() {
    let counter = 0;
    let selectedProducts = this.state.selectedProducts;
    for (var i = 0; i < selectedProducts.length; i++) {
      if (selectedProducts[i].selected) {
        counter++;
      }
    }
    return counter === 1;
  }

  renderComponents() {
    let components = [];
    for (let i = 0; i < this.props.config.components.length; i++) {
      let componentData = this.props.config.components[i];
      switch (componentData.type) {
        case "message":
          components.push(this.renderMessage(componentData, i));
          break;
        case "product":
          components.push(this.renderProduct(componentData, i));
          break;
        case "text":
          components.push(this.renderText(componentData, i));
          break;
        case "number":
          components.push(this.renderNumber(componentData, i));
          break;
        case "radio":
          components.push(this.renderRadio(componentData, i));
          break;
        case "link":
          components.push(this.renderLink(componentData, i));
          break;
        case "contactInput":
          switch (componentData.template) {
            case "firstName":
              components.push(this.renderFirstName(componentData, i));
              break;
            case "lastName":
              components.push(this.renderLastName(componentData, i));
              break;
            case "phone":
              components.push(this.renderPhoneNumber(componentData, i));
              break;
            case "email":
              components.push(this.renderEmail(componentData, i));
              break;
            case "address":
              components.push(this.renderAddress(componentData, i));
              break;
            case "city":
              components.push(this.renderCity(componentData, i));
              break;
            default:
              break;
          }
          break;
        default:
          break;
      }
    }
    return components;
  }

  renderProduct(productComponent, key) {
    return (
      <Product
        key={key}
        selectId={key}
        {...productComponent}
        selected={this.state.selectedProducts[key].selected}
        selectProduct={this.selectProduct}
        deSelectProduct={this.deSelectProduct}
        updateTotal={this.props.updateTotal}
        ref={product => {
          this[`refs-${key}`] = product;
        }}
      />
    );
  }

  renderMessage(messageComponent, key) {
    return <Message key={key} {...messageComponent} />;
  }

  renderText(textComponent, key) {
    return (
      <Text
        key={key}
        {...textComponent}
        ref={text => {
          this[`refs-${key}`] = text;
        }}
      />
    );
  }

  renderNumber(numberComponent, key) {
    return (
      <Number
        key={key}
        {...numberComponent}
        ref={num => {
          this[`refs-${key}`] = num;
        }}
      />
    );
  }

  renderRadio(radioComponent, key) {
    return <Radio key={key} {...radioComponent} />;
  }

  renderLink(linkComponent, key) {
    return <Link key={key} {...linkComponent} />;
  }

  renderFirstName(contactInputComponent, key) {
    return (
      <FirstName
        key={key}
        {...contactInputComponent}
        ref={text => {
          this[`refs-${key}`] = text;
        }}
      />
    );
  }

  renderLastName(contactInputComponent, key) {
    return (
      <LastName
        key={key}
        {...contactInputComponent}
        ref={text => {
          this[`refs-${key}`] = text;
        }}
      />
    );
  }

  renderPhoneNumber(contactInputComponent, key) {
    return (
      <Phone
        key={key}
        {...contactInputComponent}
        ref={text => {
          this[`refs-${key}`] = text;
        }}
      />
    );
  }

  renderAddress(contactInputComponent, key) {
    return (
      <Address
        key={key}
        {...contactInputComponent}
        ref={text => {
          this[`refs-${key}`] = text;
        }}
      />
    );
  }

  renderCity(contactInputComponent, key) {
    return (
      <City
        key={key}
        {...contactInputComponent}
        ref={text => {
          this[`refs-${key}`] = text;
        }}
      />
    );
  }

  renderEmail(contactInputComponent, key) {
    return (
      <Email
        key={key}
        {...contactInputComponent}
        ref={text => {
          this[`refs-${key}`] = text;
        }}
      />
    );
  }

  render() {
    return (
      <div style={this.props.style}>
        <div className="step-view">{this.renderComponents()}</div>
        <div className="button-steps">
          <button
            className="button"
            type="button"
            onClick={this.props.decrementStep}
            disabled={this.props.stepNumber === 0}
          >
            Previous
          </button>
          <button
            className="button"
            type="button"
            disabled={!this.state.valid}
            onClick={
              this.props.numOfSteps - 1 === this.props.stepNumber
                ? this.props.post
                : this.props.incrementStep
            }
          >
            {this.props.numOfSteps - 1 === this.props.stepNumber
              ? "Submit"
              : "Next"}
          </button>
        </div>
      </div>
    );
  }
}

Step.propTypes = {
  style: PropTypes.object,
  config: PropTypes.object.isRequired,
  stepNumber: PropTypes.number.isRequired,
  incrementStep: PropTypes.func.isRequired,
  decrementStep: PropTypes.func.isRequired,
  updateTotal: PropTypes.func.isRequired,
  numOfSteps: PropTypes.number.isRequired,
  post: PropTypes.func.isRequired,
  addToSelectedProducts: PropTypes.func.isRequired,
  removeFromSelectedProducts: PropTypes.func.isRequired
};

export default Step;
